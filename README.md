# Paper Statement Fee Loader

is a Java utility to kick off assess of paper statements in rabbitmq

## Running Application

No installation required. Open project in your favorite IDE add the accounts.csv in src/resources/ and run Application and it will parse the file and send signal to rabbit mq 

make sure file is in the format:

```
TenantId,AccountId,AccountNumber
4,101827690,181099134056120
4,101827690,181099134056120
```

## Getting Data From Neo4j

Currently the following query is being used in order to extract data from neo4j and copy to accounts.csv

```
match(p:IraAccount)<--(h:AccountHolder)-->(s:IraAccountSettings)
where s.digitalStatements = false and (exists (p.number)) and not (p.number = "") and not 'T23'in labels(p) and not 'T21'in labels(p) and not 'T5'in labels(p) and not 'T27'in labels(p) and not 'T16'in labels(p) and not p.number in ['1492610970199','1539891734045','1539890903638', '1539891735075','1539891732968'] 
return filter(x IN labels(p) WHERE x =~ 'T.*')[0] as tenant,id(p), p.number
```

Get Specific tenant data

```
match(p:IraAccount)<--(h:AccountHolder)-->(s:IraAccountSettings)
where s.digitalStatements = false and (exists (p.number)) and not (p.number = "") and 'T27'in labels(p) and not p.number in ['1492610970199','1539891734045','1539890903638', '1539891735075','1539891732968'] //do the checks for 5 accounts
return filter(x IN labels(p) WHERE x =~ 'T.*')[0] as tenant,id(p), p.number
```


Currently the query is ignoring tenant 23

## Pending Items

* ~~Ignore the accounts from Tenant 3~~
* ~~Double check tenants, I saw there are various tenants in prod not used~~
* ~~Get statistics of the number of accounts that will be signaled per tenant~~
* ~~Change code to drop the "T" from the tenant part will be brought in neo4j~~

## Statistics For Prod

Query used
```
match(p:IraAccount)<--(h:AccountHolder)-->(s:IraAccountSettings)
WITH filter(x IN labels(p) WHERE x =~ 'T.*')[0] as tenant, p
where s.digitalStatements = false and (exists (p.number)) and not (p.number = "") and not 'T23'in labels(p)
return tenant, count(p)
```

Results


```
╒════════╤══════════╕
│"tenant"│"count(p)"│
╞════════╪══════════╡
│"T5"    │341       │
├────────┼──────────┤
│"T8"    │528       │
├────────┼──────────┤
│"T27"   │3         │
├────────┼──────────┤
│"T33"   │4         │
├────────┼──────────┤
│"T21"   │1256      │
├────────┼──────────┤
│"T4"    │61        │
├────────┼──────────┤
│"T7"    │259       │
├────────┼──────────┤
│"T32"   │38        │
├────────┼──────────┤
│"T26"   │326       │
├────────┼──────────┤
│"T29"   │40        │
├────────┼──────────┤
│"T3"    │52        │
├────────┼──────────┤
│"T34"   │4         │
├────────┼──────────┤
│"T16"   │2         │
├────────┼──────────┤
│"T6"    │14        │
├────────┼──────────┤
│"T25"   │2005      │
├────────┼──────────┤
│"T31"   │1462      │
├────────┼──────────┤
│"T28"   │22        │
└────────┴──────────┘
```