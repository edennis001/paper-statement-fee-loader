import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.enums.CSVReaderNullFieldIndicator;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import lombok.extern.slf4j.Slf4j;
import mjson.Json;

import java.io.IOException;
import java.io.Reader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Slf4j
public class Application {

    private static final String QUEUE_NAME = "matrix-process-assess-paper-statement-fees";
    private static final String EXCHANGE_NAME = "";
    public static final String CONTENT_TYPE = "application/json";
    // rabbitmq credentials
    private static String host = "rabbitmq.dev.iralogix.local";
    private static String username = "rabbitmq-user";
    private static String password = "vSuYmbas2CUmvEBGxV2CfM";
    private static String virtualhost = "dev";
    private static int port = 5672;

    public static void main(String[] args) throws Exception {
        log.info("Starting account import..");
        readCsv(FILE_NAME)
            .stream()
            .map(Application::buildAccountJson)
            .forEach(Application::publishMessage);
    }

    // TODO move the connection so its not creating each message
    private static void publishMessage(String message) {
        log.info("Publishing message [{}]", message);
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        factory.setUsername(username);
        factory.setPassword(password);
        factory.setVirtualHost(virtualhost);
        factory.setPort(port);
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            AMQP.BasicProperties properties = new AMQP.BasicProperties.Builder()
                .contentType(CONTENT_TYPE)
                .build();
            channel.basicPublish(EXCHANGE_NAME, QUEUE_NAME, false, false, properties, message.getBytes());
        }
        catch (Exception e) {
            log.error("Unable to publish message", e);
        }
    }

    private static String buildAccountJson(String[] account) {
        Json message = Json.object().set("TenantId", removeTenantPrefix(account[0]))
            .set("AccountId", account[1])
            .set("AccountNumber", account[2]);

        return message.toString();
    }

    private static List<String[]> readCsv(String url) throws URISyntaxException, IOException {
        Reader reader = Files.newBufferedReader(Paths.get(
            ClassLoader.getSystemResource(url).toURI()));

        CSVReader csvReader = new CSVReaderBuilder(reader)
            .withFieldAsNull(CSVReaderNullFieldIndicator.EMPTY_SEPARATORS)
            // Skip the header
            .withSkipLines(1)
            .build();

        List<String[]> list = csvReader.readAll();
        reader.close();
        csvReader.close();
        log.info("Processed file [{}]. Found [{}] accounts", url, list.size());
        return list;
    }

    private static String removeTenantPrefix(String in) {
        return in.startsWith("T") ? in.substring(1) : in;
    }

}
